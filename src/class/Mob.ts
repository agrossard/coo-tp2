import { Coordinates } from '../interfaces/Coordinates';
import { Entity, Weight, Height } from '../interfaces/Entity';
import { Item } from './Item';

export class Mob implements Entity {
  id: number;
  type: string;
  health: number;
  manaLevel: number;
  forceLevel: number;
  coordinates: Coordinates;
  items: Item[];
  weight: Weight;
  height: Height;

  constructor(
    id: number,
    type: string,
    health: number,
    manaLevel: number,
    forceLevel: number,
    coordinates: Coordinates,
    items: Item[],
    weight: Weight,
    height: Height
  ) {
    this.id = id;
    this.type = type;
    this.health = health;
    this.manaLevel = manaLevel;
    this.forceLevel = forceLevel;
    this.coordinates = coordinates;
    this.items = items;
    this.weight = weight;
    this.height = height;
  }
}
