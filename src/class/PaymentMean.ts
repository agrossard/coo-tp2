import { PaymentType } from '../enums/PaymentType';

export class PaymentMean {
  #id: string;
  #type: PaymentType;
  #informations: Record<string, unknown>;

  constructor(
    id: string,
    type: PaymentType,
    informations: Record<string, unknown>
  ) {
    this.#id = id;
    this.#type = type;
    this.#informations = informations;
  }

  set id(value: string) {
    this.#id = value;
  }

  get id(): string {
    return this.#id;
  }

  set type(value: PaymentType) {
    this.#type = value;
  }

  get type(): PaymentType {
    return this.#type;
  }

  set informations(value: Record<string, unknown>) {
    this.#informations = value;
  }

  get informations(): Record<string, unknown> {
    return this.#informations;
  }
}
