import { Card } from '../interfaces/Card';
export class News implements Card {
  #id: string;
  url: string;
  title: string;
  description: string;
  image: string;

  constructor(
    id: string,
    url: string,
    title: string,
    description: string,
    image: string
  ) {
    this.#id = id;
    this.url = url;
    this.title = title;
    this.description = description;
    this.image = image;
  }

  set id(value: string) {
    this.#id = value;
  }
  get id(): string {
    return this.#id;
  }
}
