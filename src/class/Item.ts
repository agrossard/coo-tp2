import { Attribute } from '../interfaces/Attribute';

export class Item {
  id: string;
  name: string;
  price: number;
  type: string;
  attributes: Attribute[];

  constructor(
    id: string,
    name: string,
    price: number,
    type: string,
    attributes: Attribute[]
  ) {
    this.id = id;
    this.name = name;
    this.price = price;
    this.type = type;
    this.attributes = attributes;
  }
}
