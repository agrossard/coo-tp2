import { User } from '../interfaces/User';
import { Character } from './Character';
import { PaymentMean } from './PaymentMean';
import { Purchase } from './Purchase';

export class UserV1 implements User {
  wallet: PaymentMean[];
  purchases: Purchase[];
  id: number;
  name: string;
  selectedCharacter: Character;
  characters: Character[];

  constructor(
    wallet: PaymentMean[],
    purchases: Purchase[],
    id: number,
    name: string,
    selectedCharacter: Character,
    characters: Character[]
  ) {
    this.wallet = wallet;
    this.purchases = purchases;
    this.id = id;
    this.name = name;
    this.selectedCharacter = selectedCharacter;
    this.characters = characters;
  }
}
