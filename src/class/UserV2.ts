import { User } from '../interfaces/User';
import { Character } from './Character';
import { PaymentMean } from './PaymentMean';
import { Purchase } from './Purchase';

export class UserV2 implements User {
  wallet: PaymentMean[];
  purchases: Purchase[];
  id: string;
  old_id: number;
  username: string;
  selectedCharacter: Character;
  characters: Character[];

  constructor(
    wallet: PaymentMean[],
    purchases: Purchase[],
    id: string,
    old_id: number,
    username: string,
    selectedCharacter: Character,
    characters: Character[]
  ) {
    this.wallet = wallet;
    this.purchases = purchases;
    this.id = id;
    this.old_id = old_id;
    this.username = username;
    this.selectedCharacter = selectedCharacter;
    this.characters = characters;
  }
}
