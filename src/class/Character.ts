import { Coordinates } from '../interfaces/Coordinates';
import { Entity, Weight, Height } from '../interfaces/Entity';
import { Item } from './Item';

export class Character implements Entity {
  id: number;
  name: string;
  health: number;
  goldAmount: number;
  manaLevel: number;
  forceLevel: number;
  coordinates: Coordinates;
  items: Item[];
  weight: Weight;
  height: Height;

  constructor(
    id: number,
    name: string,
    health: number,
    goldAmount: number,
    manaLevel: number,
    forceLevel: number,
    coordinates: Coordinates,
    items: Item[],
    weight: Weight,
    height: Height
  ) {
    this.id = id;
    this.name = name;
    this.health = health;
    this.goldAmount = goldAmount;
    this.manaLevel = manaLevel;
    this.forceLevel = forceLevel;
    this.coordinates = coordinates;
    this.items = items;
    this.weight = weight;
    this.height = height;
  }
}
