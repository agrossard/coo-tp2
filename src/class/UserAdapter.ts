import { UserV1 } from './UserV1';
import { UserV2 } from './UserV2';
import { v4 } from 'uuid';

const userV1ToUserV2 = (userV1: UserV1): UserV2 => {
  const userV2 = new UserV2(
    userV1.wallet,
    userV1.purchases,
    v4(),
    userV1.id,
    userV1.name,
    userV1.selectedCharacter,
    userV1.characters
  );
  return userV2;
};

export { userV1ToUserV2 };
