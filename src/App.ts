import express, { Application, json, Router, urlencoded } from 'express';
import morgan from 'morgan';
import cors from 'cors';

export class App {
  readonly #instance: Application;

  constructor(readonly router: Router) {
    this.#instance = express();
    this.registerMiddlewares();
    this.registerRoutes();
  }

  public get instance(): Application {
    return this.#instance;
  }

  private registerMiddlewares(): void {
    this.#instance.use(json());
    this.#instance.use(urlencoded({ extended: false }));
    this.#instance.use(morgan('tiny'));
    this.#instance.use(cors());
  }

  private registerRoutes(): void {
    this.#instance.use(this.router);
  }
}
