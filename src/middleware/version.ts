import { Request, Response } from 'express';

const saveVersion = async (req: Request, res: Response, next: any) => {
  res.locals.version = req.params.version;
  next();
};

export default { saveVersion };
