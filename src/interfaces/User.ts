import { Character } from '../class/Character';
import { PaymentMean } from '../class/PaymentMean';
import { Purchase } from '../class/Purchase';

export interface User {
  wallet: PaymentMean[];
  purchases: Purchase[];
  selectedCharacter: Character;
  characters: Character[];
}
