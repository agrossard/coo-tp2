export interface Card {
  id: string;
  url: string;
  title: string;
  description: string;
  image: string;
}
