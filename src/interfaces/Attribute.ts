export interface Attribute {
  type: string;
  value: number;
}
