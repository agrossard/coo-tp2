import { Coordinates } from './Coordinates';
import { Item } from '../class/Item';

export interface Entity {
  id: number;
  health: number;
  manaLevel: number;
  forceLevel: number;
  coordinates: Coordinates;
  items: Item[];
  weight: Weight;
  height: Height;
}

export interface Weight {
  unit: string;
  value: number;
}

export interface Height {
  unit: string;
  value: number;
}
