import { Application } from 'express';
import { createServer, Server } from 'http';

export class HttpServer {
  readonly #port: number;

  readonly #server: Server;

  constructor(port: number, app: Application) {
    this.#port = port;
    this.#server = createServer(app);
  }

  start() {
    this.#server.listen(this.#port);
  }

  public get port() {
    return this.#port;
  }
}
