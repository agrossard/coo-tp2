import { HttpServer } from './Server';
import { App } from './App';
import { createRouter } from './routes/router';
import { initStorage } from './storage';
import './data';

const server = new HttpServer(3000, new App(createRouter()).instance);

initStorage()
  .then(() => server.start())
  .catch(() => console.error('Impossible to init storage !'));
