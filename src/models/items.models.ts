import storage from 'node-persist';

import { Item } from '../class/Item';

export async function getItems(): Promise<Item[]> {
  let items: Item[] = new Array();
  let dataItems: any[] = await storage.getItem('items');

  dataItems.forEach((dataItem: any) => {
    items.push(
      new Item(
        dataItem.id,
        dataItem.name,
        dataItem.price,
        dataItem.type,
        dataItem.attributes
      )
    );
  });
  return items;
}

export function getItemsByIDs(items: Item[], idItems: string[]): Item[] {
  let filteredItems: Item[] = new Array();

  idItems.forEach((idItem: string) => {
    filteredItems.push(getItem(items, idItem));
  });

  return filteredItems;
}

export function getItem(items: Item[], idItem: string): Item {
  let item = items.filter((element) => element.id == idItem)[0];

  return item;
}

export function setItems(items: Item[]) {
  storage.setItem('items', items);
}
