import storage from 'node-persist';

import { Character } from '../class/Character';
import { getItems, getItemsByIDs } from './items.models';
import { Item } from '../class/Item';

export async function getCharacters(): Promise<Character[]> {
  let dataCharacters: any[] = await storage.getItem('characters');
  const items = await getItems();

  let characters: Character[] = new Array();

  dataCharacters.forEach((dataCharacter: any) => {
    characters.push(
      new Character(
        dataCharacter.id,
        dataCharacter.name,
        dataCharacter.health,
        dataCharacter.goldAmount,
        dataCharacter.manaLevel,
        dataCharacter.forceLevel,
        dataCharacter.coordinates,
        getItemsByIDs(items, dataCharacter.items),
        dataCharacter.weight,
        dataCharacter.height
      )
    );
  });

  return characters;
}

export function getCharacter(
  characters: Character[],
  idCharacter: string
): Character {
  let character = characters.filter(
    (element) => element.id == parseInt(idCharacter)
  )[0];

  return character;
}

export function getCharactersByIDs(
  characters: Character[],
  idCharacters: string[]
): Character[] {
  let filteredCharacters: Character[] = new Array();

  idCharacters.forEach((idCharacter: string) => {
    filteredCharacters.push(getCharacter(characters, idCharacter));
  });

  return filteredCharacters;
}

export function setCharacters(characters: Character[]) {
  characters.forEach((character: Character) => {
    let idItems: string[] = new Array();
    try {
      character.items.forEach((item: Item) => {
        idItems.push(item.id);
      });
    } catch {
    } finally {
      character.items = idItems as any[];
    }
  });
  storage.setItem('characters', characters);
}
