import storage from 'node-persist';
import { Mob } from '../class/Mob';
import { getItems, getItemsByIDs } from '../models/items.models';

export async function getMobs(): Promise<Mob[]> {
  let dataMobs: any[] = await storage.getItem('mobs');
  const items = await getItems();

  let mobs: Mob[] = new Array();

  dataMobs.forEach((dataMob: any) => {
    mobs.push(
      new Mob(
        dataMob.id,
        dataMob.type,
        dataMob.health,
        dataMob.manaLevel,
        dataMob.forceLevel,
        dataMob.coordinates,
        getItemsByIDs(items, dataMob.items),
        dataMob.weight,
        dataMob.height
      )
    );
  });

  return mobs;
}
