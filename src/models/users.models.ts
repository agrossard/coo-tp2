import { db } from '../db';
import storage from 'node-persist';

import { UserV2 } from '../class/UserV2';
import { UserV1 } from '../class/UserV1';
import {
  getCharacter,
  getCharacters,
  getCharactersByIDs,
} from '../models/characters.models';
import { userV1ToUserV2 } from '../class/UserAdapter';

export async function getUsers(): Promise<UserV2[]> {
  const characters = await getCharacters();

  let dataUsers: any[] = new Array();
  let users: UserV2[] = new Array();

  dataUsers = await storage.getItem('users');

  dataUsers.forEach((dataUser: any) => {
    const userV1 = new UserV1(
      [],
      [],
      dataUser.id,
      dataUser.name,
      getCharacter(characters, dataUser.selectedCharacter),
      getCharactersByIDs(characters, dataUser.characters)
    );
    users.push(userV1ToUserV2(userV1));
  });

  dataUsers = db.get('users').value();

  dataUsers.forEach((dataUser: any) => {
    users.push(
      new UserV2(
        [],
        [],
        dataUser.id,
        dataUser.old_id,
        dataUser.username,
        getCharacter(characters, dataUser.playedCharacter),
        getCharactersByIDs(characters, dataUser.characters)
      )
    );
  });

  return users;
}

export function getUser(users: UserV2[], idUser: string): UserV2 {
  let user = users.filter((element) => element.old_id == parseInt(idUser))[0];

  return user;
}
