import { Request, Response } from 'express';
import { Item } from '../class/Item';
import {
  getCharacters,
  getCharacter,
  setCharacters,
} from '../models/characters.models';
import { getItem, getItems } from '../models/items.models';

const getAllCharacters = async (_req: Request, res: Response) => {
  const characters = await getCharacters();
  res.json({ characters });
};

const getOneCharacter = async (req: Request, res: Response) => {
  const { id } = req.params;

  try {
    const characters = await getCharacters();

    const character = getCharacter(characters, id);

    if (!character) {
      throw new Error('Not found');
    }

    res.json({ character });
  } catch (err) {
    res.sendStatus(404);
  }
};

const updateCharacter = async (req: Request, res: Response) => {
  const { id } = req.params;

  try {
    let characters = await getCharacters();
    const {
      name,
      health,
      goldAmount,
      manaLevel,
      forceLevel,
      coordinates,
      weight,
      height,
    } = req.body;

    let character = getCharacter(characters, id);

    if (character === undefined) {
      throw new Error('Not found');
    }
    try {
      character.name = name === undefined ? character.name : name;
      character.health = health === undefined ? character.health : health;
      character.goldAmount =
        goldAmount === undefined ? character.goldAmount : goldAmount;
      character.manaLevel =
        manaLevel === undefined ? character.manaLevel : manaLevel;
      character.forceLevel =
        forceLevel === undefined ? character.forceLevel : forceLevel;
      character.coordinates.x =
        coordinates.x === undefined ? character.coordinates.x : coordinates.x;

      character.coordinates.y =
        coordinates.y === undefined ? character.coordinates.y : coordinates.y;
      character.coordinates.z =
        coordinates.z === undefined ? character.coordinates.z : coordinates.z;
      character.weight.value =
        weight.value === undefined ? character.weight.value : weight.value;

      character.height.value =
        height.value === undefined ? character.height.value : height.value;
    } catch (err) {}

    setCharacters(characters);

    res.json({ character });
  } catch (err) {
    res.sendStatus(404);
  }
};

const getCharacterItems = async (req: Request, res: Response) => {
  try {
    const { id } = req.params;

    const characters = await getCharacters();

    const character = getCharacter(characters, id);

    res.json({ items: character.items });
  } catch (err) {
    res.sendStatus(404);
  }
};

const setCharacterItem = async (req: Request, res: Response) => {
  try {
    const { id } = req.params;
    let characters = await getCharacters();
    let character = getCharacter(characters, id);
    const { item } = req.body;

    if (typeof item !== 'string') {
      throw new Error('Bad request');
    }

    const items = await getItems();

    if (getItem(items, item) === undefined) {
      throw new Error('Not found');
    }

    character.items.push(getItem(items, item));

    setCharacters(characters);

    characters = await getCharacters();
    character = getCharacter(characters, id);

    res.send({ character });
  } catch (err) {
    res.sendStatus(404);
  }
};

const deleteCharacterItem = async (req: Request, res: Response) => {
  try {
    const { id } = req.params;

    let characters = await getCharacters();

    let character = getCharacter(characters, id);

    const { item } = req.body;

    if (typeof item !== 'string') {
      throw new Error('Bad request');
    }

    character.items.forEach((oneItem: Item, index: any) => {
      oneItem.id == item ? character.items.splice(index, 1) : false;
    });

    setCharacters(characters);

    characters = await getCharacters();
    character = getCharacter(characters, id);

    res.send({ character });
  } catch (err) {
    res.sendStatus(404);
  }
};

export default {
  getAllCharacters,
  getOneCharacter,
  getCharacterItems,
  updateCharacter,
  setCharacterItem,
  deleteCharacterItem,
};
