import { Request, Response } from 'express';
import { Item } from '../class/Item';
import { Attribute } from '../interfaces/Attribute';
import { getItem, getItems, setItems } from '../models/items.models';

const getAllItems = async (_req: Request, res: Response) => {
  const items = await getItems();
  res.json({ items });
};
const createItem = async (req: Request, res: Response) => {
  const items = await getItems();

  try {
    let { name, price, type } = req.body;
    let attributes = [
      {
        type: '',
        value: '',
      },
    ];

    if (req.body.attributes !== undefined) {
      attributes = req.body.attributes;
    }

    if (
      typeof name !== 'string' ||
      typeof price !== 'number' ||
      typeof type !== 'string' ||
      typeof attributes !== 'object' ||
      typeof attributes[0].type !== 'string' ||
      typeof attributes[0].value !== 'string'
    ) {
      throw new Error('Bad request');
    }

    let nextItemID: number = 0;

    items.forEach((item: Item) => {
      nextItemID =
        nextItemID >= parseInt(item.id.split('_')[1])
          ? nextItemID
          : parseInt(item.id.split('_')[1]);
    });

    let typedAttributes: Attribute[] = new Array();

    attributes.forEach((element: { type: string; value: string }) => {
      typedAttributes.push({
        type: element.type,
        value: parseInt(element.value),
      });
    });

    let item = new Item(
      'item_' + (nextItemID + 1),
      name,
      price,
      type,
      typedAttributes
    );

    items.push(item);

    setItems(items);

    res.json({ items });
  } catch (err) {
    res.sendStatus(400);
  }
};

const deleteItem = async (req: Request, res: Response) => {
  try {
    let items = await getItems();
    const { item } = req.body;

    let foundItem = getItem(items, item);

    if (foundItem === undefined) {
      throw new Error('Not found');
    }

    items.splice(
      items.findIndex((item) => item === foundItem),
      1
    );

    setItems(items);

    res.json({ items });
  } catch (err) {
    res.sendStatus(404);
  }
};

export default { getAllItems, createItem, deleteItem };
