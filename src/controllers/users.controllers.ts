import { Request, Response } from 'express';
import { getUsers, getUser } from '../models/users.models';

const getAllUsers = async (_req: Request, res: Response) => {
  let users = await getUsers();
  res.json({ users });
};

const getOneUser = async (req: Request, res: Response) => {
  const { id } = req.params;

  try {
    let users = await getUsers();

    const user = getUser(users, id);

    if (!user) {
      throw new Error('Not found');
    }

    res.json({ user });
  } catch (err) {
    res.sendStatus(404);
  }
};

const getUserCharacters = async (req: Request, res: Response) => {
  const { id } = req.params;

  try {
    let users = await getUsers();

    const user = getUser(users, id);

    if (!user) {
      throw new Error('Not found');
    }

    res.json({ characters: user.characters });
  } catch (err) {
    res.sendStatus(404);
  }
};

export default { getAllUsers, getOneUser, getUserCharacters };
