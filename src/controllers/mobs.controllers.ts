import { Request, Response } from 'express';
import { getMobs } from '../models/mobs.models';

const getAllMobs = async (_req: Request, res: Response) => {
  const mobs = await getMobs();
  res.json({ mobs });
};

export default { getAllMobs };
