import { Router } from 'express';
import { itemsControllers } from '../controllers';

const router = Router();

router.get('/', itemsControllers.getAllItems);

router.post('/', itemsControllers.createItem);

router.delete('/', itemsControllers.deleteItem);

export default router;
