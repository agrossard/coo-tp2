import { Router } from 'express';
import { usersControllers } from '../controllers';

const router = Router();

router.get('/', usersControllers.getAllUsers);

router.get('/:id', usersControllers.getOneUser);

router.get('/:id/characters', usersControllers.getUserCharacters);

export default router;
