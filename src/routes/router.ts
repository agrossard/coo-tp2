import { Router } from 'express';
import charactersRoutes from './characters.routes';
import usersRoutes from './users.routes';
import itemsRoutes from './items.routes';
import mobsRouters from './mobs.routes';
import versionMiddleware from '../middleware/version';
export function createRouter() {
  const router = Router();

  router.use('/:version/users', versionMiddleware.saveVersion, usersRoutes);
  router.use(
    '/:version/characters',
    versionMiddleware.saveVersion,
    charactersRoutes
  );
  router.use('/:version/items', versionMiddleware.saveVersion, itemsRoutes);
  router.use('/:version/mobs', versionMiddleware.saveVersion, mobsRouters);

  return router;
}
