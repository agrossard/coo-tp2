import { Router } from 'express';
import { mobsControllers } from '../controllers';

const router = Router();

router.get('/', mobsControllers.getAllMobs);

export default router;
