import { Router } from 'express';
import { charactersControllers } from '../controllers';

const router = Router();

router.get('/', charactersControllers.getAllCharacters);

router.get('/:id', charactersControllers.getOneCharacter);

router.post('/:id', charactersControllers.updateCharacter);

router.get('/:id/items', charactersControllers.getCharacterItems);

router.put('/:id/items', charactersControllers.setCharacterItem);

router.delete('/:id/items', charactersControllers.deleteCharacterItem);

export default router;
