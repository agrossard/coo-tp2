import storage from 'node-persist';
import { db } from './db';
import { initStorage } from './storage';

(async () => {
  await initStorage();

  await storage.setItem('users', [
    {
      id: 2,
      name: 'jeankevindu62',
      selectedCharacter: '69',
      characters: ['69', '70'],
    },
  ]);

  await storage.setItem('characters', [
    {
      id: 69,
      name: 'Tapefort',
      goldAmount: 1500,
      health: 100,
      manaLevel: 100,
      forceLevel: 100,
      coordinates: {
        x: -1,
        y: 42,
        z: 984,
      },
      items: ['item_1'],
      weight: {
        unit: 'kg',
        value: 75.8,
      },
      height: {
        unit: 'cm',
        value: 177,
      },
    },
    {
      id: 70,
      name: 'Pasouf',
      goldAmount: 0,
      health: 10,
      manaLevel: 2,
      forceLevel: 1,
      coordinates: {
        x: 0,
        y: 0,
        z: 0,
      },
      items: ['item_2'],
      weight: {
        unit: 'kg',
        value: 75.8,
      },
      height: {
        unit: 'cm',
        value: 177,
      },
    },
  ]);

  await storage.setItem('items', [
    {
      id: 'item_1',
      name: 'Epée magique',
      price: 25,
      type: 'SWORD',
      attributes: [
        {
          type: 'mana',
          value: '+50',
        },
        {
          type: 'intelligence',
          value: '-25',
        },
      ],
    },
    {
      id: 'item_2',
      type: 'SWORD',
      name: 'Epée de merde en bois',
      price: 25,
    },
    {
      id: 'item_3',
      name: 'Bâton qui fait des trucs',
      price: 6,
      type: 'STICK',
      attributes: [
        {
          type: 'mana',
          value: '+10',
        },
      ],
    },
  ]);

  await storage.setItem('mobs', [
    {
      id: 1,
      type: 'ARCHER',
      health: 100,
      manaLevel: 100,
      forceLevel: 100,
      coordinates: {
        x: -1,
        y: 42,
        z: 984,
      },
      items: ['item_1'],
      weight: {
        unit: 'kg',
        value: 75.8,
      },
      height: {
        unit: 'cm',
        value: 177,
      },
    },
    {
      id: 2,
      type: 'WIZARD',
      health: 100,
      manaLevel: 100,
      forceLevel: 100,
      coordinates: {
        x: -1,
        y: 42,
        z: 984,
      },
      items: ['item_1'],
      weight: {
        unit: 'kg',
        value: 75.8,
      },
      height: {
        unit: 'cm',
        value: 177,
      },
    },
    {
      id: 3,
      type: 'ORC',
      health: 100,
      manaLevel: 100,
      forceLevel: 100,
      coordinates: {
        x: -1,
        y: 42,
        z: 984,
      },
      items: ['item_1'],
      weight: {
        unit: 'kg',
        value: 75.8,
      },
      height: {
        unit: 'cm',
        value: 177,
      },
    },
  ]);

  db.default({
    users: [
      {
        id: 'e27d5269-0da0-4d4f-a31b-6d5a592555e8', // UUID v4
        old_id: 1,
        username: 'jeankevindu59',
        playedCharacter: 69,
        characters: [69, 70],
      },
    ],
  });
})();
