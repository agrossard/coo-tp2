export enum PaymentType {
  PAYPAL = 'PAYPAL',
  CREDITCARD = 'CREDITCARD',
  GIFTCARD = 'GIFTCARD',
}
